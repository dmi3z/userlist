const listBlock = document.querySelector('.list');
const modal = document.querySelector('.modal');
const closeModalBtn = document.querySelector('.close');
const modalWindow = document.querySelector('.modal-window');

const addFields = document.querySelectorAll('.add-field');
const addButton = document.querySelector('.add-btn');
const openModalBtn = document.querySelector('.add-user');

const genderDropdown = document.querySelector('.gender');
const ascBtn = document.querySelector('.asc');
const descBtn = document.querySelector('.desc');

const xhr = new XMLHttpRequest();

let users = [];

listBlock.classList.add('loading');
listBlock.addEventListener('click', onListClick);
closeModalBtn.addEventListener('click', closeModal);
modal.addEventListener('click', closeModal);
modalWindow.addEventListener('click', stopProp);
addButton.addEventListener('click', addUser);
openModalBtn.addEventListener('click', () => modal.classList.add('show'));
genderDropdown.addEventListener('change', changeGender);
ascBtn.addEventListener('click', sortAsc);
descBtn.addEventListener('click', sortDesc);

xhr.open('GET', 'https://randomuser.me/api?results=100');

xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            const response = xhr.responseText;
            const data = JSON.parse(response);
            users = data.results;
            renderUsers(data.results);
            renderGenderOptions(data.results);
            listBlock.classList.remove('loading');
        }
    }
}

xhr.send();


function getUserCard(user) {
    const card = getTag('div', 'user');
    const info = getTag('div', 'info');
    const fullName = user.name.first + ' ' + user.name.last;
    const name = getTag('span', 'name', fullName);
    const email = getTag('span', 'email', user.email);
    const age = getTag('span', 'age', user.dob.age);
    const avatar = getTag('img', 'avatar');
    const deleteBtn = getTag('div', 'delete', 'X');
    const editBtn = getTag('div', 'edit');

    const nameField = getTag('input', 'field');
    nameField.value = fullName;
    const emailField = getTag('input', 'field');
    emailField.value = user.email;
    const ageField = getTag('input', 'field');
    ageField.value = user.dob.age;

    avatar.src = user.picture.medium;
    card.id = user.login.uuid;

    info.append(name, email, age, nameField, emailField, ageField);
    card.append(avatar, info, deleteBtn, editBtn);

    return card;
}

function getTag(tagName, className, content = null) {
    const newTag = document.createElement(tagName);
    newTag.classList.add(className);
    newTag.innerText = content;

    return newTag;
}

function renderUsers(users) {
    listBlock.innerHTML = '';
    const userCards = users.map(item => getUserCard(item));
    listBlock.append(...userCards);
}

function onListClick(event) {
    if (event.target.className === 'delete') {
        const id = event.target.parentNode.id;
        deleteUser(id);
    }

    if (event.target.className === 'edit') {
        const infoBlock = event.target.parentNode.childNodes[1];
        const className = infoBlock.className;
        const isEditable = className.includes('editable');
        if (isEditable) {
            infoBlock.classList.remove('editable');
            const inputs = [...infoBlock.childNodes].slice(3);
            saveChanges(inputs, event.target.parentNode.id);
        } else {
            infoBlock.classList.add('editable');
        }
    }
}

function deleteUser(userId) {
    users = users.filter(item => item.login.uuid !== userId);
    renderUsers(users);
}

function saveChanges([name, email, age], id) {
    const user = users.find(item => item.login.uuid === id);
    user.email = email.value;
    user.dob.age = age.value;

    const nameArr = name.value.split(' ');
    user.name.first = nameArr[0] ? nameArr[0] : 'Empty';
    user.name.last = nameArr[1] ? nameArr[1] : '';

    renderUsers(users);
}

function closeModal() {
    modal.classList.remove('show');
}

function stopProp(event) {
    event.stopPropagation();
    // preventDefault() - отключает стандартное поведение
}

function addUser() {
    const [name, email, age] = addFields;

    const patternEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/gmi;
    const patternName = /^([a-z]+ [a-z]+)$/gmi;
    const patternAge = /^(?:1(?:00?|\d)|[2-5]\d|[6-9]\d?)$/gm;

    const isEmailValid = patternEmail.test(email.value);
    const isNameValid = patternName.test(name.value);
    const isAgeValid = patternAge.test(age.value);

    clearModalErrors();
    if (isEmailValid && isNameValid && isAgeValid) {
        const newUser = createUser(name.value, email.value, age.value);
        users.push(newUser);
        renderUsers(users);
        clearModalFields();
        closeModal();
        scrollToLast();
    } else {
        name.classList.add(isNameValid ? 'valid' : 'error');
        email.classList.add(isEmailValid ? 'valid' : 'error');
        age.classList.add(isAgeValid ? 'valid' : 'error');
    }
}

function clearModalErrors() {
    addFields.forEach(field => field.classList.remove('error'));
}

function createUser(name, email, age) {
    const [first, last] = name.split(' ');
    const newUser = {
        email,
        dob: {
            age
        },
        name: {
            first,
            last
        },
        login: {
            uuid: Math.random().toString()
        },
        picture: {
            medium: './images/stub.jpg'
        }
    };
    return newUser;
}

function clearModalFields() {
    addFields.forEach(item => item.value = '');
}

function scrollToLast() {
    setTimeout(() => {
        const lastIndex = listBlock.children.length - 1;
        listBlock.children[lastIndex].scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 0);
}


function changeGender(event) {
    const gender = event.target.value;
    if (gender === 'all') {
        renderUsers(users);
    } else {
        const filteredUsers = users.filter(item => item.gender === gender);
        renderUsers(filteredUsers);
    }
}

function renderGenderOptions(users) {
    const allGenders = users.map(item => item.gender);
    const uniqueGenders = Array.from(new Set(allGenders));
    uniqueGenders.unshift('all');

    const optionTags = uniqueGenders.map(item => {
        const option = getTag('option', 'option', item);
        option.value = item;

        return option;
    });

    genderDropdown.append(...optionTags);
}

function sortAsc() {
    const newUsers = [...users];
    // newUsers.sort((a, b) => a.dob.age - b.dob.age);
    newUsers.sort((a, b) => a.name.first.localeCompare(b.name.first));
    renderUsers(newUsers);
}

function sortDesc() {
    // users.sort((a, b) => b.dob.age - a.dob.age);
    users.sort((a, b) => b.name.first.localeCompare(a.name.first));
    renderUsers(users);
}
